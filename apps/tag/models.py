from django.db import models
from task.models import Task


class Tag(models.Model):

    def __unicode__(self):
        return self.title

    task = models.ForeignKey(Task, blank=True, null=True)
    title = models.CharField(u'Task title', max_length=255)
