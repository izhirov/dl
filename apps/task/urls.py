from django.conf.urls import patterns, include, url
from .views import *


urlpatterns = patterns('',
    url(r'^$', index_page, name="index-page"),
    url(r'^export_document/$', export_document, name="export-document"),
)