import datetime
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Task(models.Model):

    def __unicode__(self):
        return self.title

    parent_task = models.ForeignKey('self', blank=True, null=True, related_name='subtasks')

    title = models.CharField(u'Task title', max_length=255)

    deadline = models.DateField(u"Deadline", null=True, blank=True)
    planned_deadline = models.DateField(u"Planned Deadline", null=True, blank=True)
    actual_deadline = models.DateField(u"Actual Deadline", null=True, blank=True)

    is_done = models.BooleanField(default=0)

    def save(self, *args, **kwargs):
        self.actual_deadline = None
        if self.is_done:
            self.actual_deadline = datetime.datetime.now()

        super(Task, self).save()

    class Meta:
        ordering = ['-pk']

