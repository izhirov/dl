from apps.task.forms import TaskForm
from apps.task.models import Task
from rest_framework import viewsets,  serializers
from rest_framework.decorators import action
from rest_framework.response import Response


class SubTaskSerializator(serializers.ModelSerializer):
    class Meta:
        model = Task


class TaskSerializator(serializers.ModelSerializer):
    parent_task_id = serializers.PrimaryKeyRelatedField(source='parent_task',
                                                        required=False,
                                                        read_only=True)
    subtasks = SubTaskSerializator(many=True,
                                   required=False,
                                   read_only=True)

    class Meta:
        model = Task
        fields = [
            'id',
            'deadline',
            'planned_deadline',
            'actual_deadline',
            'subtasks',
            'parent_task_id',
            'title',
            'is_done'
        ]


class TaskViewSet(viewsets.ModelViewSet):
    model = Task
    serializer_class = TaskSerializator

    def list(self, request):
        queryset = Task.objects.filter(parent_task__isnull=True)
        serializer = TaskSerializator(queryset, many=True)
        return Response(serializer.data)

    @action()
    def add_subtask(self, request, pk=None):

        subtask = SubTaskSerializator(data=request.DATA)

        if subtask.is_valid():
            task_subtask = subtask.save()
            task_subtask.parent_task = self.get_object()
            task_subtask.save()

            return Response(subtask.data)

        return Response({'Errors': subtask.errors})


