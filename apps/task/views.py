
from annoying.decorators import render_to
from apps.task.models import Task
from apps.task.xml_parser import DocumentExport, TaskParser
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response


@render_to("index.html")
def index_page(request):
    return {}


def export_document(request):
    exporter = DocumentExport(parser=TaskParser)

    response = HttpResponse(mimetype="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=report.xls'

    exporter.get_document(queryset=Task.objects.filter(parent_task__isnull=True)).save(response)

    return response