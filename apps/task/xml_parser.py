# coding=utf-8

import datetime
from pyExcelerator import Workbook


# helper for escaping different types of fields
def escape_field(field=None):
    if isinstance(field, datetime.date):
        return datetime.datetime.strftime(field, "%Y-%m-%d")

    if isinstance(field, bool):
        return u"Завершена" if field else u"Не завершена"

    return field


class TaskParser(object):
    fields = (
        (u"Название задачи", "title"),
        (u"Дедлайн", "deadline"),
        (u"Планируемый срок", "planned_deadline"),
        (u"Реальный срок завершения", "actual_deadline"),
        (u"Закончена", "is_done")
    )

    def _create_document(self):
        self.workbook_lists.append(self.workbook.add_sheet(u"Выборка задач"))

    def _write_headers(self):

        _column, _row = 0, 0

        for field in self.fields:
            self.active_list.write(_column, _row, field[0])
            _row += 1

    def create_document(self, queryset=None):

        _column = 1

        for _model in queryset:

            _row = 0
            for _field in self.fields:

                if not hasattr(_model, _field[1]):
                    continue

                if getattr(_model, _field[1]):
                    attr = escape_field(getattr(_model, _field[1]))
                    self.active_list.write(_column, _row, attr)

                _row += 1

            _column += 1

        return self.workbook

    def get_workbook(self, queryset):
        pass

    def __init__(self):
        self.workbook = Workbook()
        self.workbook_lists = list()
        self._create_document()
        self.active_list = self.workbook_lists[0]

        self._write_headers()


class DocumentExport(object):
    def __init__(self, parser=None):
        self.parser = parser()

    def get_document(self, queryset=None):
        return self.parser.create_document(queryset=queryset)


