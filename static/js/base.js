var angular = angular || {};

var TaskApp = angular.module('app', ['chieffancypants.loadingBar', 'ngCookies', 'ngResource', 'ngRoute']);
TaskApp.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{*');
  $interpolateProvider.endSymbol('*}');
});

TaskApp.run(function ($http, $cookies) {
    $http.defaults.headers.common['X-CSRFToken'] = $cookies['csrftoken'];
});

TaskApp.directive('datepicker', function($parse) {
  var directiveDefinitionObject = {
    restrict: 'A',
    link: function postLink(scope, iElement, iAttrs) {
      iElement.datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function(dateText, inst) {
          scope.$apply(function(scope){
            $parse(iAttrs.ngModel).assign(scope, dateText);
          });
        }
      });
    }
  };
  return directiveDefinitionObject;
});