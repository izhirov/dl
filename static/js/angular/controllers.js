
function TaskController($scope, Task) {

    $scope.tasks = Task.query();
    $scope.task = new Task();

    $scope.time = "1212-12-12";

    $scope.saveTask = function() {
        Task.save($scope.task, function(data) {
            $scope.tasks.unshift(data);
            $scope.task = new Task();
        });
    }

    $scope.deleteTask = function(idx) {
        Task.delete({id:$scope.tasks[idx].id}, function() {
            $scope.tasks.splice(idx, 1);
        });
    }

    $scope.editTask = function(idx) {

        var task = $scope.tasks[idx];

        if (typeof task.edditing === 'undefined') {
            task.edditing = false;
        }

        // update after end edditing
        if(task.edditing) {
            $scope.updateTask(task);
        }

        task.edditing = !task.edditing;
    }

    $scope.addSubtask = function(task) {
        Task.saveSubtask({id:task.id, action:'add_subtask'}, task.subtask, function(data) {
           task.subtasks.unshift(data);
           task.subtask = new Task();
        });
    }

    $scope.deleteSubTask = function(task, subtask) {
        Task.delete({id:subtask.id}, function() {
            task.subtasks.splice(task.subtasks.indexOf(subtask), 1);
        });
    }

    $scope.updateTask = function(task) {
        Task.update({id: task.id}, task, function(data){
            task.actual_deadline = data.actual_deadline;
        });
    }
}
