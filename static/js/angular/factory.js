/**
 * Created by ivanzhirov on 25.04.14.
 */

TaskApp.factory('Task', ['$resource', function($resource) {
    return $resource('/api/tasks/:id/:action', {}, {
        update: {
          method: 'PUT'
        },
        saveSubtask: {
            method: 'POST'
        }
    });
}]);