from apps.task.api import TaskViewSet
from django.conf.urls import patterns, include, url

from django.contrib import admin
from rest_framework import routers

admin.autodiscover()

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'tasks', TaskViewSet)

urlpatterns = patterns('',
                       url(r'^api/', include(router.urls)),

                       url(r'^', include("task.urls")),
                       url(r'^admin/', include(admin.site.urls)),
)
